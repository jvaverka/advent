defmodule Day00 do
  defp read_input(example) do
    filename = if example, do: "example", else: "input"
    dirname = :escript.script_name() |> Path.dirname()

    File.read!("#{dirname}/#{filename}.txt")
    |> String.split()
  end

  def part1(example \\ false) do
    example
    |> read_input()
  end

  def part2(example \\ false) do
    example
    |> read_input()
  end
end

IO.inspect(Day00.part1(), label: "Part 1")
IO.inspect(Day00.part2(), label: "Part 2")
