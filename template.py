import pathlib


def read_input(example=False) -> list:
    filename = 'example' if example else 'input'
    dirname = pathlib.Path(__file__).parent.resolve()
    with open(f'{dirname}/{filename}.txt') as file:
        input = file.readlines()
    return input


def part1(example: bool):
    input = read_input(example=example)
    return input


def part2(example: bool):
    input = read_input(example=example)
    return input


print('Part 1:', part1(example=False))
print('Part 2:', part2(example=False))
