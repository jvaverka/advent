const fs = require('node:fs');
const path = require('path');

function readInput(example = false) {
    let data = null;
    const filename = example ? 'example' : 'input';
    const dirname = path.dirname(__filename);
    const filepath = `${dirname}/${filename}.txt`;
    try {
        data = fs.readFileSync(filepath, 'utf8');
    } catch (error) {
        console.error(error);
    }
    return data;
}

function parseInput(input) {
    return [...input.matchAll(/mul\((\d+),(\d+)\)/g)]
}

function parseInputConditional(input) {
    const matches = [...input.matchAll(/mul\((\d+),(\d+)\)|do\(\)|don\'t\(\)/g)]
    let state = 'enabled';
    const muls = [];
    for (let match of matches) {
        if (match[0].startsWith('do()')) {
           state = 'enabled';
        }
        if (match[0].startsWith('don\'t()')) {
           state = 'disabled';
        }
        if (state == 'enabled' && match[0].startsWith('mul')) {
            muls.push(parseInt(match[1]) * parseInt(match[2]));
        }
    }
    return muls
}

function part1(example) {
    const input = readInput(example);
    const memory = parseInput(input);
    return memory.reduce((acc, curr) => acc + parseInt(curr[1]) * parseInt(curr[2]), 0);
}

function part2(example) {
    const input = readInput(example);
    const memory = parseInputConditional(input);
    return memory.reduce((acc, curr) => acc + curr, 0);
}

console.log(`Part 1: ${part1()}`);
console.log(`Part 2: ${part2()}`);
