defmodule Day03 do
  defp read_input(example) do
    filename = if example, do: "example", else: "input"
    dirname = :escript.script_name() |> Path.dirname()

    File.read!("#{dirname}/#{filename}.txt")
  end

  def part1(example \\ false) do
    input = read_input(example)

    Regex.scan(~r/mul\((\d+),(\d+)\)/, input)
    |> Enum.map(fn match ->
      String.to_integer(Enum.at(match, 1)) * String.to_integer(Enum.at(match, 2))
    end)
    |> Enum.sum()
  end

  def part2(example \\ false) do
    input = read_input(example)

    Regex.scan(~r/mul\((\d+),(\d+)\)|do\(\)|don\'t\(\)/, input)
    |> Enum.map(fn match ->
      case match do
        ["do()"] -> {:enable}
        ["don't()"] -> {:disable}
        [_, a, b] -> {:mul, String.to_integer(a) * String.to_integer(b)}
      end
    end)
    |> Enum.reduce({:enabled, 0}, fn curr, {state, total} ->
      case curr do
        {:enable} -> {:enabled, total}
        {:disable} -> {:disabled, total}
        {:mul, _} when state == :disabled -> {:disabled, total}
        {:mul, value} when state == :enabled -> {:enabled, total + value}
      end
    end)
    |> Kernel.elem(1)
  end
end

IO.inspect(Day03.part1(), label: "Part 1")
IO.inspect(Day03.part2(), label: "Part 2")
