from functools import reduce
from itertools import chain
import pathlib
import re


def read_input(example=False) -> list:
    filename = "example" if example else "input"
    dirname = pathlib.Path(__file__).parent.resolve()
    with open(f"{dirname}/{filename}.txt") as file:
        input = file.readlines()
    return "".join(input)


def parse_input(input: str) -> list:
    regex = r"mul\(\d+,\d+\)"
    memory = re.findall(regex, input)
    return [mem for mem in memory]


def parse_input_conditional(input: str) -> list:
    dos = [split for split in re.split(r"do\(\)", input)]
    enabled = []
    for do in dos:
        splits = re.split(r"don't\(\)", do)
        chunk = splits[0]
        enabled.append(chunk)
    regex = r"mul\((\d+),(\d+)\)"
    memory = [re.findall(regex, do) for do in enabled]
    return list(chain.from_iterable(memory))


def part1(example: bool) -> int:
    input = read_input(example=example)
    memory = parse_input(input)
    pairs = [re.findall(r"\d+", mul) for mul in memory]
    return reduce(lambda acc, pair: acc + int(pair[0]) * int(pair[1]), pairs, 0)


def part2(example: bool) -> int:
    input = read_input(example=example)
    memory = parse_input_conditional(input)
    return reduce(lambda acc, pair: acc + int(pair[0]) * int(pair[1]), memory, 0)


print("Part 1:", part1(example=False))
print("Part 2:", part2(example=False))
