import pathlib


def read_input(example=False) -> str:
    filename = "example" if example else "input"
    dirname = pathlib.Path(__file__).parent.resolve()
    with open(f"{dirname}/{filename}.txt") as file:
        input = file.read().strip()
    return input


def part1(example: bool) -> int:
    disk = []
    file_id = 0
    for i, char in enumerate(read_input(example=example)):
        size = int(char)
        if i % 2 == 0:
            disk += [file_id] * size
            file_id += 1
        else:
            disk += [-1] * size
    blanks = [i for i, char in enumerate(disk) if char == -1]
    for index in blanks:
        while disk[-1] == -1: disk.pop()                                  # NOTE: remove trailing free space
        if len(disk) <= index: break                                      # NOTE: stop after reducing disk
        disk[index] = disk.pop()
    return sum([index * file_id for index, file_id in enumerate(disk)])


def part2(example: bool) -> int:
    files = {}
    blanks = []
    file_id = 0
    position = 0
    for i, char in enumerate(read_input(example=example)):
        size = int(char)
        if i % 2 == 0:
            files[file_id] = (position, size)
            file_id += 1
        else:
            if size != 0:                                                 # NOTE: guard against empty spaces
                blanks.append((position, size))
        position += size                                                  # NOTE: move forward the file size
    while file_id > 0:                                                    # NOTE: use existing `file_id`
        file_id -= 1
        position, size = files[file_id]
        for i, (start, length) in enumerate(blanks):
            if start >= position:                                         # NOTE: remove blanks after rightmost file
                blanks = blanks[:i]
                break
            if size <= length:
                files[file_id] = (start, size)
                if size == length:
                    blanks.pop(i)
                else:
                    blanks[i] = (start + size, length - size)
                break
    total = 0
    for fid, (position, size) in files.items():
        for x in range(position, position + size):
            total += fid * x
    return total


print("Part 1:", part1(example=False))
print("Part 2:", part2(example=False))
