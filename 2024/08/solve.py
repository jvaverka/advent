import collections
import itertools
import pathlib
import string


def read_input(example=False) -> list:
    filename = "example" if example else "input"
    dirname = pathlib.Path(__file__).parent.resolve()
    with open(f"{dirname}/{filename}.txt") as file:
        input = [line.strip() for line in file.readlines()]
    return input


def parse_input(input) -> dict:
    grid = {}
    for y in range(len(input)):
        for x in range(len(input[0])):
            grid[x, y] = input[y][x]
    return grid


def find_antennas(grid: dict) -> dict:
    antennas = collections.defaultdict(set)
    frequencies = "".join([string.ascii_letters, string.digits])
    for position in grid.keys():
        for frequency in frequencies:
            if grid[position] == frequency:
                antennas[frequency].add(position)
    return antennas


def possible_antinodes(x1: int, y1: int, x2: int, y2: int, n: int, m: int):
    x = 2 * x1 - x2
    y = 2 * y1 - y2
    if 0 <= x <= n and 0 <= y <= m:
        yield x, y


def possible_harmonic_antinodes(
    x1: int, y1: int, x2: int, y2: int, n: int, m: int, mul
):
    x = x2 + (mul * (x1 - x2))
    y = y2 + (mul * (y1 - y2))
    if 0 <= x <= n and 0 <= y <= m:
        yield x, y


def find_antinodes(grid: dict, antennas: dict) -> set[tuple]:
    antinodes = set()
    n = max([x for x, y in grid.keys()])
    m = max([y for x, y in grid.keys()])
    for antenna in antennas.values():
        for a, b in itertools.combinations(antenna, 2):
            antinodes.update(possible_antinodes(*a, *b, n, m))
            antinodes.update(possible_antinodes(*b, *a, n, m))
    return antinodes


def find_harmonic_antinodes(grid: dict, antennas: dict) -> set[tuple]:
    antinodes = set()
    n = max([x for x, y in grid.keys()])
    m = max([y for x, y in grid.keys()])
    for antenna in antennas.values():
        for a, b in itertools.combinations(antenna, 2):
            antinodes.add(a)
            antinodes.add(b)
            for i in range(2, m):
                antinodes.update(possible_harmonic_antinodes(*a, *b, n, m, i))
                antinodes.update(possible_harmonic_antinodes(*b, *a, n, m, i))
    return antinodes


def part1(example: bool) -> int:
    input = read_input(example)
    grid = parse_input(input)
    antennas = find_antennas(grid)
    antinodes = find_antinodes(grid, antennas)
    return len(antinodes)


def part2(example: bool) -> int:
    input = read_input(example)
    grid = parse_input(input)
    antennas = find_antennas(grid)
    antinodes = find_harmonic_antinodes(grid, antennas)
    return len(antinodes)


print("Part 1:", part1(example=False))
print("Part 2:", part2(example=False))
