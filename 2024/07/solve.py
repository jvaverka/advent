import pathlib


def read_input(example=False) -> list:
    filename = "example" if example else "input"
    dirname = pathlib.Path(__file__).parent.resolve()
    with open(f"{dirname}/{filename}.txt") as file:
        input = [line.strip() for line in file.readlines()]
    return input


def parse_input(input: list) -> list:
    calibrations = []
    for calibration in input:
        value, numbers = calibration.split(":")
        value = int(value)
        numbers = [int(number) for number in numbers.strip().split(" ")]
        calibrations.append((value, numbers))
    return calibrations


def true_test_values(calibrations: list, cat=False) -> list:
    true_values = []
    for value, numbers in calibrations:
        lefts = [numbers.pop(0)]
        while numbers:
            temp = []
            right = numbers.pop(0)
            for left in lefts:
                if cat:
                    temp.append(int(f"{left}{right}"))
                temp.append(left + right)
                temp.append(left * right)
            lefts = temp
        if value in lefts:
            true_values.append(value)
    return true_values


def part1(example: bool) -> int:
    input = read_input(example=example)
    calibrations = parse_input(input)
    true_values = true_test_values(calibrations)
    return sum(true_values)


def part2(example: bool) -> int:
    input = read_input(example=example)
    calibrations = parse_input(input)
    true_values = true_test_values(calibrations, cat=True)
    return sum(true_values)


print("Part 1:", part1(example=False))
print("Part 2:", part2(example=False))
