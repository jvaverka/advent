from collections import Counter
import pathlib


def read_input(example=False) -> list:
    filename = "example" if example else "input"
    dirname = pathlib.Path(__file__).parent.resolve()
    with open(f"{dirname}/{filename}.txt") as file:
        input = file.readlines()
    return input


def parse_input(input):
    lines = [line.split() for line in input]
    left = [int(line[0]) for line in lines]
    right = [int(line[1]) for line in lines]
    return sorted(left), sorted(right)


def part1(example: bool) -> int:
    input = read_input(example=example)
    left, right = parse_input(input)
    return sum([abs(left[i] - right[i]) for i in range(len(left))])


def part2(example: bool) -> int:
    input = read_input(example=example)
    left, right = parse_input(input)
    freq = Counter(right)
    return sum([loc * freq[loc] for loc in left])


print("Part 1:", part1(example=False))
print("Part 2:", part2(example=False))
