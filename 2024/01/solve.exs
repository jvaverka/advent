defmodule Day01 do
  defp parse_input(example) do
    filename = if example, do: "example", else: "input"
    dirname = :escript.script_name() |> Path.dirname()

    File.read!("#{dirname}/#{filename}.txt")
    |> String.split()
    |> Enum.map(fn loc -> String.to_integer(loc) end)
    |> Enum.with_index()
    |> Enum.split_with(fn {_value, index} -> rem(index, 2) == 0 end)
    |> then(fn {left, right} ->
      {Enum.map(left, fn {value, _index} -> value end) |> Enum.sort(),
       Enum.map(right, fn {value, _index} -> value end) |> Enum.sort()}
    end)
  end

  def part1(example \\ false) do
    example
    |> parse_input()
    |> then(fn {left, right} ->
      left
      |> Enum.with_index()
      |> Enum.map(fn {value, index} -> abs(value - Enum.at(right, index)) end)
      |> Enum.sum()
    end)
  end

  def part2(example \\ false) do
    example
    |> parse_input()
    |> then(fn {left, right} ->
      freq = Enum.frequencies(right)

      left
      |> Enum.map(fn loc -> loc * Map.get(freq, loc, 0) end)
      |> Enum.sum()
    end)
  end
end

IO.inspect(Day01.part1(), label: "Part 1")
IO.inspect(Day01.part2(), label: "Part 2")
