const fs = require('node:fs');
const path = require('path');

function readInput(example = false) {
    let data = null;
    const filename = example ? 'example' : 'input';
    const dirname = path.dirname(__filename);
    const filepath = `${dirname}/${filename}.txt`;
    try {
        data = fs.readFileSync(filepath, 'utf8');
    } catch (error) {
        console.error(error);
    }
    return data;
}

function parseInput(input) {
    const lines = input.split(/\n/).filter((line) => line !== '');
    const left = lines.map((line) => parseInt(line.split(/\s+/)[0]));
    const right = lines.map((line) => parseInt(line.split(/\s+/)[1]));
    return [left.sort(), right.sort()];
}

function part1(example) {
    const input = readInput(example);
    const [left, right] = parseInput(input);
    return Array.from(left.entries()).reduce((acc, [index, value]) => {
        return acc + Math.abs(value - right[index])
    }, 0);
}

function part2(example) {
    const input = readInput(example);
    const [left, right] = parseInput(input);
    const frequencies = right.reduce((acc, curr) => {
        return acc[curr] ? ++acc[curr] : acc[curr] = 1, acc
    }, {})
    return left.reduce((acc, curr) => {
        return frequencies[curr]
            ? acc + curr * frequencies[curr]
            : acc
    }, 0);
}

console.log(`Part 1: ${part1()}`);
console.log(`Part 2: ${part2()}`);
