import pathlib
from itertools import dropwhile, takewhile
from functools import reduce


def read_input(example=False) -> list:
    filename = "example" if example else "input"
    dirname = pathlib.Path(__file__).parent.resolve()
    with open(f"{dirname}/{filename}.txt") as file:
        input = [line.strip() for line in file.readlines()]
    return input


def parse_rules(rules: list) -> dict:
    result = {}
    for rule in rules:
        before, after = rule.split("|")
        b = int(before)
        a = int(after)
        if not b in result.keys():
            result[b] = [a]
        else:
            result[b].append(a)
    return result


def parse_updates(pages: list) -> list:
    return list(
        map(lambda update: list(map(lambda page: int(page), update.split(","))), pages)
    )


def parse_input(input: list) -> tuple[dict, list]:
    rules = list(takewhile(lambda rule: "|" in rule, input))
    updates = list(dropwhile(lambda rule: "|" in rule or rule == "", input))
    return parse_rules(rules), parse_updates(updates)


def is_correct(update: list, rules: dict) -> bool:
    for i in range(len(update) - 1):
        curr = update[i]
        for j in range(i + 1, len(update)):
            next = update[j]
            if next in rules.keys() and curr in rules[next]:
                return False
    return True


def correct(update: list, rules: dict) -> list:
    corrected = update.copy()
    if is_correct(corrected, rules):
        return corrected
    for i in range(len(update) - 1):
        curr = update[i]
        for j in range(i + 1, len(update)):
            next = update[j]
            if next in rules.keys() and curr in rules[next]:
                corrected[i] = next
                corrected[j] = curr
                return correct(corrected, rules)
    return []


def part1(example: bool) -> int:
    input = read_input(example=example)
    rules, updates = parse_input(input)
    correct_updates = list(filter(lambda update: is_correct(update, rules), updates))
    return reduce(
        lambda acc, update: acc + update[len(update) // 2], correct_updates, 0
    )


def part2(example: bool) -> int:
    input = read_input(example=example)
    rules, updates = parse_input(input)
    incorrect_updates = list(
        filter(lambda update: not is_correct(update, rules), updates)
    )
    corrected_updates = [(correct(update, rules)) for update in incorrect_updates]
    return reduce(
        lambda acc, update: acc + update[len(update) // 2], corrected_updates, 0
    )


print("Part 1:", part1(example=False))
print("Part 2:", part2(example=False))
