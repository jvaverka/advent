defmodule Day05 do
  defp read_input(example) do
    filename = if example, do: "example", else: "input"
    dirname = :escript.script_name() |> Path.dirname()

    File.stream!("#{dirname}/#{filename}.txt")
    |> Enum.map(&String.split(&1))
    |> Enum.reject(&Enum.empty?/1)
    |> Enum.map(fn [line] ->
      cond do
        String.contains?(line, "|") ->
          [first, second] = String.split(line, "|")
          {:rule, {String.to_integer(first), String.to_integer(second)}}

        String.contains?(line, ",") ->
          new_updates =
            line
            |> String.split(",")
            |> Enum.map(&String.to_integer/1)

          {:update, new_updates}
      end
    end)
    |> Enum.reduce(%{rules: %{}, updates: []}, fn item, acc ->
      case item do
        {:rule, {first, second}} ->
          update_in(acc, [:rules, first], fn existing ->
            case existing do
              nil -> [second]
              _ -> [second | existing]
            end
          end)

        {:update, updates} ->
          Map.update(acc, :updates, [updates], &[updates | &1])
      end
    end)
  end

  defp is_correct?(update, rules) do
    for i <- 0..length(update), j <- (i + 1)..length(update) do
      curr = Enum.at(update, i)
      next = Enum.at(update, j)

      if next in Map.keys(rules) and curr in Map.get(rules, next) do
        false
      else
        true
      end
    end
    |> Enum.all?(&(&1 == true))
  end

  def part1(example \\ false) do
    %{rules: rules, updates: updates} = read_input(example)

    Enum.filter(updates, &is_correct?(&1, rules))
    |> Enum.reduce(0, fn curr, acc -> acc + Enum.at(curr, div(length(curr), 2)) end)
  end

  def part2(example \\ false) do
    read_input(example)
  end
end

IO.inspect(Day05.part1(), label: "Part 1")
# IO.inspect(Day05.part2(), label: "Part 2")
