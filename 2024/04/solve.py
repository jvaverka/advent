import pathlib


def read_input(example=False) -> list:
    filename = "example" if example else "input"
    dirname = pathlib.Path(__file__).parent.resolve()
    with open(f"{dirname}/{filename}.txt") as file:
        input = file.readlines()
    return [line.strip() for line in input]


def parse_input(input: list) -> dict:
    grid = {}
    for y in range(len(input)):
        for x in range(len(input[0])):
            grid[x, y] = input[y][x]
    return grid


def deltas(x: int, y: int, xmax: int, ymax: int) -> list:
    dxy = [[0, 1], [1, 1], [1, 0], [1, -1], [0, -1], [-1, -1], [-1, 0], [-1, 1]]
    return [
        [dx, dy]
        for xy in dxy
        if 0 <= (dx := x + xy[0]) <= xmax and 0 <= (dy := y + xy[1]) <= ymax
    ]


def diagnals(x: int, y: int, xmax: int, ymax: int) -> list:
    dxy = [[1, 1], [1, -1], [-1, -1], [-1, 1]]
    return [
        [dx, dy]
        for xy in dxy
        if 0 <= (dx := x + xy[0]) <= xmax and 0 <= (dy := y + xy[1]) <= ymax
    ]


def _search_all_neighbors(grid: dict, x: int, y: int, ch: str, m: int, n: int) -> list:
    matches = []
    for x2, y2 in deltas(x, y, m, n):
        if grid[x2, y2] == ch:
            matches.append([x2, y2])
    return matches


def _search_next_neighbor(grid: dict, x: int, y: int, ch: str, m: int, n: int) -> list:
    if not 0 <= x <= m or not 0 <= y <= n:
        return []
    elif grid[x, y] != ch:
        return []
    else:
        return [x, y]


def _next(first: int, second: int) -> int:
    return second + (second - first)


def search_xmas(grid: dict) -> list:
    words = []
    m = max([xy[0] for xy in grid.keys()])
    n = max([xy[1] for xy in grid.keys()])
    for y in range(n + 1):
        for x in range(m + 1):
            if grid[x, y] == "X":
                if mxys := _search_all_neighbors(grid, x, y, "M", m, n):
                    for mxy in mxys:
                        if axy := _search_next_neighbor(
                            grid, _next(x, mxy[0]), _next(y, mxy[1]), "A", m, n
                        ):
                            if sxy := _search_next_neighbor(
                                grid,
                                _next(mxy[0], axy[0]),
                                _next(mxy[1], axy[1]),
                                "S",
                                m,
                                n,
                            ):
                                words.append([(x, y), sxy])
    return words


def _x_words(ms: list, ss: list, x: int, y: int) -> bool:
    m_corners = [(m[0] - x, y - m[1]) for m in ms]
    s_corners = [(s[0] - x, y - s[1]) for s in ss]
    if (
        ((1, 1) in m_corners and (-1, -1) not in s_corners)
        or ((1, -1) in m_corners and (-1, 1) not in s_corners)
        or ((-1, -1) in m_corners and (1, 1) not in s_corners)
        or ((-1, 1) in m_corners and (1, -1) not in s_corners)
    ):
        return False
    return True


def search_x_mas(grid: dict) -> list:
    words = []
    m = max([xy[0] for xy in grid.keys()])
    n = max([xy[1] for xy in grid.keys()])
    for y in range(n + 1):
        for x in range(m + 1):
            if grid[x, y] == "A":
                diags = diagnals(x, y, m, n)
                m_matches = [
                    match
                    for diag in diags
                    if (
                        match := _search_next_neighbor(
                            grid, diag[0], diag[1], "M", m, n
                        )
                    )
                    != []
                ]
                s_matches = [
                    match
                    for diag in diags
                    if (
                        match := _search_next_neighbor(
                            grid, diag[0], diag[1], "S", m, n
                        )
                    )
                    != []
                ]
                if (
                    len(m_matches) == 2
                    and len(s_matches) == 2
                    and _x_words(m_matches, s_matches, x, y)
                ):
                    words.append((x, y))
    return words


def part1(example: bool) -> int:
    input = read_input(example=example)
    grid = parse_input(input)
    return len(search_xmas(grid))


def part2(example: bool) -> int:
    input = read_input(example=example)
    grid = parse_input(input)
    return len(search_x_mas(grid))


print("Part 1:", part1(example=False))
print("Part 2:", part2(example=False))
