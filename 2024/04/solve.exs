defmodule Day04 do
  defp read_input(example) do
    filename = if example, do: "example", else: "input"
    dirname = :escript.script_name() |> Path.dirname()

    lines =
      File.read!("#{dirname}/#{filename}.txt")
      |> String.split()

    for y <- 0..(length(lines) - 1), x <- 0..(byte_size(Enum.at(lines, y)) - 1), into: %{} do
      {{x, y}, String.at(Enum.at(lines, y), x)}
    end
  end

  defp search_neighbors(grid, x, y, char) do
    [{0, 1}, {1, 1}, {1, 0}, {1, -1}, {0, -1}, {-1, -1}, {-1, 0}, {-1, 1}]
    |> Enum.map(fn {dx, dy} -> {x + dx, y + dy} end)
    |> Enum.filter(fn key -> Map.fetch(grid, key) == {:ok, char} end)
  end

  defp get_diagonals(grid, {x, y}) do
    [{1, 1}, {1, -1}, {-1, -1}, {-1, 1}]
    |> Enum.map(fn {dx, dy} -> {x + dx, y + dy} end)
    |> Enum.filter(fn key -> Map.fetch(grid, key) != :error end)
  end

  defp step_next({x1, y1}, {x2, y2}) do
    {x2 + (x2 - x1), y2 + (y2 - y1)}
  end

  defp search_next(grid, x, y, char) do
    case Map.fetch(grid, {x, y}) do
      {:ok, ^char} -> {x, y}
      _ -> nil
    end
  end

  def part1(example \\ false) do
    grid = read_input(example)

    for y <- 0..Enum.max(for key <- Map.keys(grid), do: elem(key, 0)),
        x <- 0..Enum.max(for key <- Map.keys(grid), do: elem(key, 1)) do
      if Map.get(grid, {x, y}) == "X" do
        mxys = search_neighbors(grid, x, y, "M")

        for {mx, my} <- mxys do
          {ax, ay} = step_next({x, y}, {mx, my})
          axy = search_next(grid, ax, ay, "A")

          unless axy == nil do
            {sx, sy} = step_next({mx, my}, {ax, ay})
            sxy = search_next(grid, sx, sy, "S")

            unless sxy == nil do
              sxy
            end
          end
        end
      end
    end
    |> Enum.reject(&is_nil/1)
    |> Enum.flat_map(&Enum.reject(&1, fn element -> is_nil(element) end))
    |> Enum.count()
  end

  defp diagonal?(as, bs, {x, y}) do
    a_corners = for a <- as, do: {elem(a, 0) - x, y - elem(a, 1)}
    b_corners = for b <- bs, do: {elem(b, 0) - x, y - elem(b, 1)}

    if ({1, 1} in a_corners and {-1, -1} not in b_corners) or
         ({1, -1} in a_corners and {-1, 1} not in b_corners) or
         ({-1, -1} in a_corners and {1, 1} not in b_corners) or
         ({-1, 1} in a_corners and {1, -1} not in b_corners) do
      false
    else
      true
    end
  end

  def part2(example \\ false) do
    grid = read_input(example)

    for y <- 0..Enum.max(for key <- Map.keys(grid), do: elem(key, 0)),
        x <- 0..Enum.max(for key <- Map.keys(grid), do: elem(key, 1)) do
      if Map.get(grid, {x, y}) == "A" do
        coords = get_diagonals(grid, {x, y})

        m_matches =
          coords
          |> Enum.map(fn {x, y} -> search_next(grid, x, y, "M") end)
          |> Enum.reject(&is_nil/1)

        s_matches =
          coords
          |> Enum.map(fn {x, y} -> search_next(grid, x, y, "S") end)
          |> Enum.reject(&is_nil/1)

        if length(m_matches) == 2 and
             length(s_matches) == 2 and
             diagonal?(m_matches, s_matches, {x, y}) do
          {x, y}
        end
      end
    end
    |> Enum.reject(&is_nil/1)
    |> Enum.count()
  end
end

IO.inspect(Day04.part1(), label: "Part 1")
IO.inspect(Day04.part2(), label: "Part 2")
