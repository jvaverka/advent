defmodule Day02 do
  defp parse_input(example) do
    filename = if example, do: "example", else: "input"
    dirname = :escript.script_name() |> Path.dirname()

    File.read!("#{dirname}/#{filename}.txt")
    |> String.split("\n", trim: true)
    |> Enum.map(fn line ->
      line
      |> String.split(" ")
      |> Enum.map(&String.to_integer(&1))
    end)
  end

  defp is_safe(report) do
    report
    |> Enum.chunk_every(2, 1, :discard)
    |> Enum.map(fn [left, right] ->
      diff = left - right

      %{
        ascending: diff < 0 and 1 <= abs(diff) and abs(diff) <= 3,
        descending: diff > 0 and 1 <= abs(diff) and abs(diff) <= 3
      }
    end)
    |> then(fn report_map ->
      Enum.any?([
        Enum.all?(Enum.map(report_map, &Map.get(&1, :ascending))),
        Enum.all?(Enum.map(report_map, &Map.get(&1, :descending)))
      ])
    end)
  end

  defp is_safe(report, _dampened) do
    0..(length(report) - 1)
    |> Enum.map(&List.pop_at(report, &1))
    |> Enum.map(&elem(&1, 1))
    |> Enum.map(&is_safe(&1))
    |> Enum.any?()
  end

  def part1(example \\ false) do
    example
    |> parse_input()
    |> Enum.map(&is_safe(&1))
    |> Enum.filter(fn safety -> safety end)
    |> Enum.count()
  end

  def part2(example \\ false) do
    example
    |> parse_input()
    |> Enum.map(&is_safe(&1, true))
    |> Enum.filter(fn safety -> safety end)
    |> Enum.count()
  end
end

IO.inspect(Day02.part1(), label: "Part 1")
IO.inspect(Day02.part2(), label: "Part 2")
