const fs = require('node:fs');
const path = require('path');

function readInput(example = false) {
    let data = null;
    const filename = example ? 'example' : 'input';
    const dirname = path.dirname(__filename);
    const filepath = `${dirname}/${filename}.txt`;
    try {
        data = fs.readFileSync(filepath, 'utf8');
    } catch (error) {
        console.error(error);
    }
    return data;
}

function parseInput(input) {
    return input
        .split(/\n/)
        .filter(row => row !== '')
        .map(report => report.split(/\s/))
        .map(report => report.map(level => parseInt(level)));
}

function isSafe(report) {
    const decreasing = [];
    const increasing = [];
    for (let i = 0; i < report.length - 1 ; i++) {
        const diff = report[i] - report[i + 1];
        decreasing.push(1 <= diff && diff <= 3);
        increasing.push(1 <= -diff && -diff <= 3);
    }
    return decreasing.every(result => result === true)
        || increasing.every(result => result === true)
}

function isSafeDampened(report) {
    const n = report.length;
    const anySafe = [];
    for (let i = 0; i < n; i++) {
        const newReport = report.slice(0, i).concat(report.slice(i + 1, n));
        anySafe.push(isSafe(newReport));
    }
    return isSafe(report) || anySafe.some(result => result);
}

function part1(example) {
    const input = readInput(example);
    const reports = parseInput(input);
    return reports
        .map(report => isSafe(report))
        .filter(bool => bool).length;
}

function part2(example) {
    const input = readInput(example);
    const reports = parseInput(input);
    return reports
        .map(report => isSafeDampened(report))
        .filter(bool => bool).length;
}

console.log(`Part 1: ${part1()}`);
console.log(`Part 2: ${part2()}`);
