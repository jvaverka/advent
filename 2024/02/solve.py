import pathlib


def read_input(example=False) -> list:
    filename = "example" if example else "input"
    dirname = pathlib.Path(__file__).parent.resolve()
    with open(f"{dirname}/{filename}.txt") as file:
        input = file.readlines()
    return input


def parse_input(input: list) -> list:
    return list(
        map(lambda line: list(map(lambda level: int(level), line.split())), input)
    )


# https://gist.github.com/HexTree/dbff5437231584509280098bbdaf9a68
def is_safe(report: list, dampen_problem=False) -> bool:
    n = len(report)
    if dampen_problem:
        return is_safe(report) or any(
            is_safe(report[:i] + report[i + 1 :]) for i in range(len(report))
        )
    else:
        return (all(1 <= report[i + 1] - report[i] <= 3 for i in range(n - 1))) or (
            all(1 <= report[i] - report[i + 1] <= 3 for i in range(n - 1))
        )


def part1(example=False) -> int:
    input = read_input(example=example)
    reports = parse_input(input)
    return sum([is_safe(report) for report in reports])


def part2(example=False) -> int:
    input = read_input(example=example)
    reports = parse_input(input)
    return sum([is_safe(report, dampen_problem=True) for report in reports])


print("Part 1:", part1(example=False))
print("Part 2:", part2(example=False))
