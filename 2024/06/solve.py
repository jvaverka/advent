import pathlib
from functools import reduce


def read_input(example=False) -> list:
    filename = "example" if example else "input"
    dirname = pathlib.Path(__file__).parent.resolve()
    with open(f"{dirname}/{filename}.txt") as file:
        input = [line.strip() for line in file.readlines()]
    return input


def parse_input(input: list) -> dict:
    grid = {}
    for y in range(len(input)):
        for x in range(len(input[0])):
            grid[x, y] = input[y][x]
    return grid


def rotate(heading: tuple) -> tuple:
    if heading == (0, -1):
        return (1, 0)
    elif heading == (1, 0):
        return (0, 1)
    elif heading == (0, 1):
        return (-1, 0)
    else:
        return (0, -1)


def patrol(grid: dict, start: tuple, heading: tuple) -> dict:
    grid[start] = "X"
    while (next := (start[0] + heading[0], start[1] + heading[1])) in grid.keys():
        if grid[next] == ".":
            grid[next] = "X"
            start = (next[0], next[1])
        elif grid[next] == "X":
            start = (next[0], next[1])
        elif grid[next] == "#":
            heading = rotate(heading)
    return grid


def patrol_loop(
    grid: dict, start: tuple, heading: tuple, obstacle: tuple, limit: int
) -> int:
    steps = 0
    grid[start] = "X"
    grid[obstacle] = "O"
    while (
        next := (start[0] + heading[0], start[1] + heading[1])
    ) in grid.keys() and steps < limit:
        if grid[next] == ".":
            grid[next] = "X"
            start = (next[0], next[1])
            steps += 1
        elif grid[next] == "X":
            start = (next[0], next[1])
            steps += 1
        elif grid[next] == "#" or grid[next] == "O":
            heading = rotate(heading)
    return steps


def part1(example: bool) -> int:
    input = read_input(example=example)
    grid = parse_input(input)
    start = [xy for xy in grid.keys() if grid[xy] == "^"][0]
    path = patrol(grid, start, (0, -1))
    return reduce(lambda acc, curr: acc + (1 if curr == "X" else 0), path.values(), 0)


def part2(example: bool) -> int:
    LIMIT = 10_000
    input = read_input(example=example)
    grid = parse_input(input)
    start = [xy for xy in grid.keys() if grid[xy] == "^"][0]
    path = patrol(grid.copy(), start, (0, -1))
    possibilities = [pos for pos in path.keys() if path[pos] == "X"]
    return reduce(
        lambda acc, curr: acc
        + (1 if patrol_loop(grid.copy(), start, (0, -1), curr, LIMIT) >= LIMIT else 0),
        possibilities,
        0,
    )


print("Part 1:", part1(example=False))
print("Part 2:", part2(example=False))
