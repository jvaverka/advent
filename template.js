const fs = require('node:fs');
const path = require('path');

function readInput(example = false) {
    let data = null;
    const filename = example ? 'example' : 'input';
    const dirname = path.dirname(__filename);
    const filepath = `${dirname}/${filename}.txt`;
    try {
        data = fs.readFileSync(filepath, 'utf8');
    } catch (error) {
        console.error(error);
    }
    return data;
}

function part1(example) {
    const input = readInput(example);
    return input;
}

function part2(example) {
    const input = readInput(example);
    return input;
}

console.log(`Part 1: ${part1()}`);
console.log(`Part 2: ${part2()}`);
